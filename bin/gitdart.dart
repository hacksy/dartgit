#!/usr/bin/env dart


import 'dart:io';

import 'package:args/args.dart';

import 'src/commands/add.dart';
import 'src/commands/init.dart';
import 'package:path/path.dart' as p;

ArgResults argResults;
const lineNumber = 'line-number';

void main(List<String> arguments) {
  final parser = ArgParser();
  //..addFlag(lineNumber, negatable: false, abbr: 'n');
  argResults = parser.parse(arguments);

  if (argResults.arguments.isEmpty) {
    var helpFile = File('assets/help.txt');
    print(helpFile.readAsStringSync());
  } else {
    switch (argResults.arguments[0]) {
      case 'init':
        if (argResults.arguments.length > 2) {
          var initError = File('assets/initError.txt');
          print(initError.readAsStringSync());
          return;
        }
        var path = argResults.arguments.length == 1 ? '' : argResults
            .arguments[1];
        var joined = '';
        if(path.trim().startsWith('~') || path.trim().startsWith("/")){
           joined = path;
        }else{
          joined = p.join(Directory.current.path,path);
        }
        GitInit(path:joined)..initRepo();
        break;
      case 'add':
        if (argResults.arguments.length < 2) {
          var initError = File('assets/addError.txt');
          print(initError.readAsStringSync());
          return;
        }
        var joined = Directory.current.path;
        var files =  argResults.arguments.sublist(1,argResults.arguments.length);
        GitAdd(path:joined)..addFiles(files);
        break;
      case 'commit':
        print('git add');
        break;
      case 'status':
        print('\u001B[0;32mgit add');
        break;

      case "branch":
        print('git branch');
        break;
      case "clone":
        print('git clone');
        break;
    }
  }
}
