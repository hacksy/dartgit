import 'dart:io';
import 'package:path/path.dart' as p;

class GitInit {
  String path;

  GitInit({this.path});

  void initRepo() async {
    if (!await Directory(path).exists()) {
      Directory(path).createSync(recursive: true);
    }
    var directories = [
      '.git/info/',
      '.git/hooks/',
      '.git/objects/info/',
      '.git/objects/pack/',
      '.git/refs/heads/',
      '.git/refs/tags/'
    ];
    var files = <String, String>{
      '.git/HEAD': 'ref: refs/heads/default',
      '.git/description':
          "Unnamed repository; edit this file 'description' to name the repository.",
      '.git/info/exclude':
          "# git ls-files --others --exclude-from=.git/info/exclude\n# Lines that start with '#' are comments.\n# For a project mostly in C, the following would be a good set of\n# exclude patterns (uncomment them if you want to use them):\n# *.[oa]\n# *~",
      '.git/config':
          '[core]\n      repositoryformatversion = 0\n      filemode = true\n      bare = false\n      logallrefupdates = true\n      ignorecase = true\n      precomposeunicode = true',
      '.git/hooks/applypatch-msg.sample': '',
      '.git/hooks/commit-msg.sample': '',
      '.git/hooks/fsmonitor-watchman.sample': '',
      '.git/hooks/post-update.sample': '',
      '.git/hooks/pre-applypatch.sample': '',
      '.git/hooks/pre-commit.sample': '',
      '.git/hooks/pre-merge-commit.sample': '',
      '.git/hooks/pre-push.sample': '',
      '.git/hooks/pre-rebase.sample': '',
      '.git/hooks/pre-receive.sample': '',
      '.git/hooks/prepare-commit-msg.sample': '',
      '.git/hooks/update.sample': '',
    };

    directories.forEach((element) {
      var joinedPath = p.join(path, element);
      Directory(joinedPath).createSync(recursive: true);
    });
    files.forEach((key, value) {
      var joinedPath = p.join(path, key);
      var myFile = File(joinedPath);
      myFile.writeAsString(value);
    });
    var finalPath = p.join(path,'.git/');
    print('Initialized empty Git repository in $finalPath');
  }
}
