import 'dart:convert';
import 'dart:ffi';
import 'dart:io' as io;
import 'dart:io';
import 'dart:typed_data';
import 'package:path/path.dart' as p;
import 'package:crypto/crypto.dart';

class GitAdd {
  String path;
  GitAdd({this.path});

  void addFiles(List<String> files)async{
    for (var value in files) {
      var joinedPath = p.join(path, value);
      if(await io.File(joinedPath).exists()){
        var file = File(joinedPath);
        var contents=file.readAsBytesSync();
        var content =List<int>.from(contents);
        var toHash = 'blob ${content.length}\x00';
        var bytes = utf8.encode(toHash);
        var digest = sha1.convert(bytes+content);
        var objectsPath = p.join(path,'.git/objects/');
        var joinedObjectDirPath =p.join(objectsPath,digest.toString().substring(0,2));
        var joinedObjectFilePath = p.join(objectsPath,digest.toString().substring(0,2),digest.toString().substring(2,40));
        Directory(joinedObjectDirPath).createSync(recursive: true);
        File(joinedObjectFilePath)..writeAsBytesSync( zlib.encode(bytes+content));
      }
    }
  }

}

// git hash-object -w test.txt
// find .git/objects -type f
// git cat-file -p